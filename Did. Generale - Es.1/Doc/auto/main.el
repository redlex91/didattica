(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("memoir" "a4paper" "oneside" "article" "titlepage" "10pt" "extrafontsizes" "final")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("babel" "italian") ("hyperref" "hidelinks" "colorlinks" "linkcolor=DarkSlateBlue!30!Black" "urlcolor=DarkSlateBlue!30!Black") ("xcolor" "svgnames") ("tcolorbox" "skins" "breakable")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir10"
    "fontspec"
    "microtype"
    "lipsum"
    "calc"
    "inputenc"
    "babel"
    "manfnt"
    "hyperref"
    "xcolor"
    "enumitem"
    "tcolorbox"
    "tikz"
    "fontawesome5")
   (LaTeX-add-tcolorbox-newtcolorboxes
    '("achtung" "" "" "")
    '("spoton" "1" "" "")))
 :latex)

