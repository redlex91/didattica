\changetocdepth {2}
\babel@toc {italian}{}\relax 
\babel@toc {italian}{}\relax 
\contentsline {section}{\numberline {1.1}Prospetto dell'UdA}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Titolo, discliplina di riferimento, contenuto, classe e istituto scolastico}{3}{section.1.2}%
\contentsline {section}{\numberline {1.3}Traguardi di sviluppo di competenze}{3}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Area delle competenze matematiche}{3}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Area delle competenze di cittadinanza}{3}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Area delle competenze digitali}{3}{subsection.1.3.3}%
\contentsline {section}{\numberline {1.4}Prerequisiti}{4}{section.1.4}%
\contentsline {section}{\numberline {1.5}Obiettivi}{4}{section.1.5}%
\contentsline {subsection}{\numberline {1.5.1}Livello elementare}{4}{subsection.1.5.1}%
\contentsline {subsection}{\numberline {1.5.2}Livello intermedio/superiore}{4}{subsection.1.5.2}%
\contentsline {section}{\numberline {1.6}Tecniche di insegnamento}{4}{section.1.6}%
\contentsline {section}{\numberline {1.7}Materiali didattici, risorse, spazi e tempi}{4}{section.1.7}%
\contentsline {subsection}{\numberline {1.7.1}Materiali didattici e risorse}{4}{subsection.1.7.1}%
\contentsline {subsection}{\numberline {1.7.2}Spazi e tempi}{5}{subsection.1.7.2}%
\contentsline {section}{\numberline {1.8}Percorso didattico}{5}{section.1.8}%
\contentsline {subsection}{\numberline {1.8.1}Lezione 1}{5}{subsection.1.8.1}%
\contentsline {subsection}{\numberline {1.8.2}Lezione 2}{5}{subsection.1.8.2}%
\contentsline {subsection}{\numberline {1.8.3}Lezione 3}{5}{subsection.1.8.3}%
\contentsline {subsection}{\numberline {1.8.4}Lezione 4}{6}{subsection.1.8.4}%
\contentsline {subsection}{\numberline {1.8.5}Lezione 5}{6}{subsection.1.8.5}%
\contentsline {subsection}{\numberline {1.8.6}Lezione 6}{7}{subsection.1.8.6}%
\contentsline {section}{\numberline {1.9}Valutazione formativa}{7}{section.1.9}%
\contentsline {section}{\numberline {1.10}Valutazione finale}{7}{section.1.10}%
\contentsline {section}{\numberline {1.11}Attività di recupero}{7}{section.1.11}%
\contentsline {section}{\numberline {1.12}Valutazione finale}{7}{section.1.12}%
