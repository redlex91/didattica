(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("memoir" "showtrims" "openright" "twoside")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "italian") ("xcolor" "svgnames") ("hyperref" "colorlinks=true" "linkcolor=DarkSlateBlue") ("tcolorbox" "many")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir10"
    "fontenc"
    "inputenc"
    "babel"
    "xcolor"
    "tabularx"
    "graphicx"
    "longtable"
    "pdflscape"
    "calc"
    "pdfpages"
    "amsmath"
    "amssymb"
    "fontawesome"
    "enumitem"
    "hyperref"
    "tikz"
    "tcolorbox")
   (LaTeX-add-labels
    "img:icf"
    "sec:PDP")
   (LaTeX-add-lengths
    "mywidth")
   (LaTeX-add-tcolorbox-newtcolorboxes
    '("myquotes" "" "" "")))
 :latex)

