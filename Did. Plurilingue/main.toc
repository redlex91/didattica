\babel@toc {italian}{}
\babel@toc {italian}{}
\beamer@sectionintoc {1}{Introduzione}{3}{0}{1}
\beamer@sectionintoc {2}{Lingue e linguaggi}{7}{0}{2}
\beamer@sectionintoc {3}{Contenuti}{11}{0}{3}
\beamer@subsectionintoc {3}{1}{La tematica portante}{11}{0}{3}
\beamer@subsectionintoc {3}{2}{Esempio di attività didattica}{17}{0}{3}
\beamer@sectionintoc {4}{Metodologie di lavoro}{25}{0}{4}
\beamer@subsectionintoc {4}{1}{Tipi di attività}{25}{0}{4}
\beamer@subsectionintoc {4}{2}{Schemi di interazione}{31}{0}{4}
\beamer@subsectionintoc {4}{3}{Risorse e tecnologie}{37}{0}{4}
\beamer@subsectionintoc {4}{4}{Forme di feedback}{49}{0}{4}
\beamer@sectionintoc {5}{Obiettivi di apprendimento}{55}{0}{5}
\beamer@sectionintoc {6}{Criteri e modalità di valutazione}{59}{0}{6}
