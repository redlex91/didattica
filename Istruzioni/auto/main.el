(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("memoir" "openany" "oneside" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "italian") ("xcolor" "svgnames" "table") ("hyperref" "colorlinks=true" "linkcolor=DarkSlateBlue")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir10"
    "fontenc"
    "inputenc"
    "babel"
    "xcolor"
    "array"
    "tabularx"
    "lipsum"
    "booktabs"
    "tikz"
    "hyperref")
   (LaTeX-add-labels
    "count1"
    "count2")
   (LaTeX-add-counters
    "mycount")
   (LaTeX-add-xcolor-definecolors
    "ThemeColor"))
 :latex)

